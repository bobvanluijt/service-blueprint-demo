$ Service
Vintage Robot Webshop

$ Scenario
Person buys a vintage Robot

	$ Step

			$ Step Definition
			A person searches for the website. ![Step 1](images/step1.jpg "Step 1")

			$ Channel
			Search engine

	$ Step

			$ Step Definition
			On opening the website, all robots are visible. ![Step 2](images/step2.jpg "Step 2")

			$ Channel
			Vintage robot website

			$ Channel
			Vintage robot iOS app

			$ Actor
			Somebody who wants a collectors robot

			$ API, System
			HAL

			$ Policy, rule
			Apple policy: release to the App Store

			$ Policy, rule
			Robots can't be ship to Timboektoe

			$ Question
			Do we have information about the collectors that visit the website?

			$ Idea
			We should show animated gifs of the robots instead of static jpgs

	$ Step

			$ Step Definition
			Person looks at a robot info page. ![Step 3](images/step3.jpg "Step 3")

			$ Idea
			Call to action should be well visible.

			$ Actor
			Somebody who is seriously interested in buying

	$ Step

			$ Step Definition
			Happy customer receives a robot at home! ![Step 4](images/step4.jpg "Step 4")

			$ Actor
			Lorem markdownum, ieiunia! Nodoso rite mentito ipse doctus mutatus heros, custos deae nexilibus, saucia pericula erravit. Et et alis; et vicem nymphae dabitis cervice, tonitruque inpia. Flet iter divitis [undas](http://www.youtube.com/watch?v=MghiBW3r65M), verba diversosque tibi. Facit a exhortor, quam **et litora** successore Byblis reparet. Tuos des fertilitas aras timentia avidum [Cerealia in](http://www.mozilla.org/) solet meumque.

			$ API, System
			Remolliat sceptri sedere natura puellae inquit conspectos sacra, festum, sua vero missa dieque! Aetola **tempore parentes** segetes secura trahit ferre, undas, in! Omnibus vivaci, homo opem quoque: et voveam, epulas. Resolvite nomen et [formam terebat](http://textfromdog.tumblr.com/) infamataeque paene magis, est _animos_ stimulos lacrimis, est fert!

			$ Criticalmoment
			Iovi retexuit Romana [fac](http://www.billmays.net/): obicit venenata aqua: res! Lympha ara, iussi perque fronde hic at ante **luctus**, e per, sit dum. Figis vidit scelerata inermia placeat, valeant umerosque sumptas proceres omnes Theseos litoream ut. Glaciali saxumque o Ismenos interius vestes si pater [manet](http://omgcatsinspace.tumblr.com/) certamina dubitat inpulsu et minus ubi.

			$ Idea
			Inanis et movetur curvos, poenas consequitur nautae suum **pervia semina**. Nil [ore solio](http://twitter.com/search?q=haskell) nunc. Et redit conantemque ad perque [et terrigenasque spectat](http://twitter.com/search?q=haskell) Arcas cognataque urbes. Excipit Maenalon populo ne hinc, medicamine sumit, pars duobus inter!

			$ Metric
			Eripuit a dextra? _Nil_ alvo caput tamen facit, Phoebes gaudet, Sardibus argenteus Alcmenae moenia. Quam **sunt** traiecit pulvereumque qui possem dictaque miseram in teque arma Thebis, [paterque](http://hipstermerkel.tumblr.com/) satum _deus Atlas_ incomitata obstarique.

			$ Observation, fact
			Et libido sincerae partu contigerant annis sit sua quod sumpserat calcat. Indulgens nefas Samos patitur recentes, iustae, simplex inopem. Quam montis flammis ademptum loqui poteram portantes saturos dum, est praeda dignatus, vis dubitor resisti, de ternisque.

			$ Policy, rule
			O est curam mora: auras Dorylas! **Tangat pronumque** omne illo adeunt dat tota nec, terga. Urbe tanta bracchiaque vel quippe ferebant, igni cum caelum genetrici dentibus orbis lacrimas cerae, vindicat limen nec **mea**. Ad hanc et _mala_.

			$ Question
			Praevertunt unus praetemptat erat salictis, obruta, tua odit. Parvoque et iuvenis fero monstri velox.

			$ Channel
			Nostri te attulit castra molliter virgine. Si amoris bracchia aquae non Alcyone, cognoscenti comites Aegides accedere mersae Credulitas. A velut huius rotae sevocat, fratribus posse Pana crura at o teneri, ire et emissi vellent plebe, obvia. Pestifera vim concipis plurima conantesque quantaque coniuge plenaque equo nuda cum iunctasque parum? Unda viget fabrilis forsitan utrique cacumine lacrimis stamen coepitque dici guttis, est cum ferarum haeret se celer?

